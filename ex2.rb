require 'json'
file = File.read('./pokemons.json')

ruby = JSON.parse(file)

results = ruby['results']

i = 0

#puts results[0]["name"]

nomes = []

while i < results.length
    
    if results[i]['name'].index("-") == nil
        nomes.push(results[i]['name'])
    
    elsif results[i]['name'].index("-")
        index_hifen = results[i]['name'].index("-") -1
        nomes.push(results[i]['name'][0..index_hifen])
    end
    i += 1

end

module Enumerable
    def sort_by_frequency_descending
        histogram = inject(Hash.new(0)) { |hash, x| hash[x] += 1; hash}
        sort_by { |x| [histogram[x] * -1, x]}
    end
end

frequentes = nomes.sort_by_frequency_descending

#puts frequentes

topfrequentes = []

for c in 0..frequentes.length
    if frequentes[c] != frequentes[c+1]
        topfrequentes.push(frequentes[c])
    end
end

for d in 0..10
    puts topfrequentes[d]
end


#puts nomes